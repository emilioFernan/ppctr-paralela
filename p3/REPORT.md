# P3: VideoTask
## Ejercicio 1

### Explica  que  funcionalidad  has  tenido  que  añnadir/modificar  sobre  el  codigo  original.

    Principalmente he tenido que modificar el bucle que se encarga de la lectura de las imagenes 
    y de su correspondiente filtrado:
    

```
    do
    {
     printf("%d\n",i);
      size = fread(pixels[i%seq], (height+2) * (width+2) * sizeof(int), 1, in);

      if (size)
      {

         #pragma omp task
         fgauss (pixels[i%seq], filtered[i%seq], height, width);





      }
      i++;
      if(i%seq==0){

        #pragma omp taskwait
        for(int p=0;p<seq;p++){

        fwrite(filtered[p%seq], (height+2) * (width + 2) * sizeof(int), 1, out);
        }
        i=0;
      }

    } while (!feof(in));
```
    
    En un principio la variable iterativa no incrementaba, por lo que tuve que añadir el i++.
    Otra de las cosa que tuve que modificar fue el hecho de tanto la lectura como la escritura se tenia que hacer de forma secuencial
    ya que la parte que se iba a paralelizar era la parte de aplicar el filtro a cada imagen 
    

### Explica si has encontrado algun fallo en la funcionalidad.



### Explica brevemente la funcion que desarrolla cada una de las directivas y funciones de  OpenMP  que  has  utilizado,  salvo  las  que  ya  hayas  explicado  en  la  practica anterior (por ejemplo parallel).

    #pragma omp task-> genera tareas de forma explicita que se dejan en una cola, cuando un thread esta libre toma la primera tarea y la ejecuta
    
    #pragma omp taskwait -> Es una barrera de forma que se espera a que todas las tareas generadas hayan finalizado antes de seguir 
    
    #pragma  omp single -> Determina una region la cual va a ser ejecutada por un solo thread
    
### Etiqueta todas las variables y explica por que le has asignado esa etiqueta.

    #pragma omp parallel for private(x,y,sum,dx,dy) shared(pixels,filtered,heigh, width,filter)
    
    He puesto como private aquellas variables las cuales se definen dentro de la región paralela y ademas un dato debe ser privado siempre que pueda tener
    distinto valor en distintos threads
    
    He puesto como shared aquellas variables cuyo valor tiene que ser usado por varios threads, ya que el mismo valor tiene que ser usado por varios threads
    
### Explica como se consigue el paralelismo en este programa (aplicable sobre la funcion main).
    
    En este programa, la paralelizacion se centra en la generacion de tareas correspondientes a aplicar el filtro a una serie de imagenes.
    
    El programa va leyendo una serie de imagenes de un fichero, y las va guardando en un espacio reservado de memoria el cual puede ser menor que el numero de imageens a leer
    
    La lectura se hace de manera secuencial asi como la generación ya que se iran creando tareas correspondientes al metodo que aplica el filtro a la imagen
    Esto se consigue mediante el uso de la directiva single.
    
    Para evitar que se llene y se machaque imagenes del buffer por la continua lectura de imagenes de video, lo que hago es que va generando tareas hasta que el buffer 
    esta lleno y una vez este lleno, espera a que acaben las tareas generadas y el thread de la region single escribe las imagenes filtradas en orden en movie.out y sigue leyendo imagenes y generando
    tareas hasta que no queden mas en el archivo movie.in
    
    Por lo tanto la escritura de las imagenes ya filtradas se hace tambiend de manera secuencial.
    
    Entonces el paralelismo se consigue mediante un hilo single que va generando tareas, encolandolas y estas se iran ejecutando por los otros threads
    
    
### Calcula la ganancia (speedup) obtenido con la paralelizacion y explica los resultados obtenidos.

    Para calcular la ganancia he realizado un script que ejecute un total de 10 veces tanto en secuencial como en paralelo y con esos datos
    calculare la media del tiempo de respuesta y con eso hallare la ganancia.
    
El script que he usado es el siguiente:
**pract3bench.sh**
```
#!/usr/bin/env bash
file=resultsPract3.log
if [ -f $file ];then
    rm resultsPract3.log
fi

touch $file
gcc -std=c99 -fopenmp -lm -o pract3 pract3.c
data=data.dat
  echo "Comienzo del Script"
./pract3 1>/dev/null
./pract3 1> /dev/null
for i in 1 2 3 4 5 6 7 8 9 10 ; do

  echo "Iteracion"$i>>$file
  ./pract3>> $file

done
```
    Calcule la media de cada tiempo de respuesta, en secuencial de media tarda 2.2351563 y en paralelo 1.3195315.
    Por lo que me queda una ganacia de 1.694901434. Teniendo en cuenta que uso el tamaño por defecto
    
    Por curiosidad cambie el script para probar diferentes valores de seq para ver si afectaba al tiempo de respuesta
    
```
#!/usr/bin/env bash
file=resultsPract3.log
if [ -f $file ];then
    rm resultsPract3.log
fi

touch $file
gcc -std=c99 -fopenmp -lm -o pract3 pract3.c
data=data.dat
  echo "Comienzo del Script"
./pract3 1>/dev/null
./pract3 1> /dev/null
for seq in 4 8 10 16 20 40 ; do
  echo "Parametro seq"$seq>> $file
  for i in 1 2 3 4 5 6 7 8 9 10 ; do

    echo "Iteracion"$i>>$file
    ./pract3>> $file

  done
done
```

    (Use valores de seq divisores de 80) 
    
    Los resultados que obtuve para cada valor de seq eran muy similares entre ellos y a su vez a los obtenidos a los calculados antes por lo que la ganacia
    será parecida
    
    Tambien probé a cambiar el tamaño del video utilizando un video de 1280x720, donde el tiempo medio de respuesta secuencial fue de 0.7441406
    y en paralelo de 0.3859375 por lo que hay una ganacia de 1.928137587
    Para un tamaño de 640x360 el tiempo de respuesta medio para secuencial fue de 0.1964843 y para paralelo de 0.1125 con lo que la ganancia obtenida es de 1.746527111
    

### Se te ocurre alguna optimizacion sobre la funcion fgauss? Explica que has hecho y que ganancia supone con respecto a la version paralela anterior
    La primera optimizacion que he realizado en fgauss ha sido la de desenrollado de bucles, de manera que el primer bucle hago la iteracion x y x+1 en la misma.
*Primera Optimización*
```
void fgauss (int *pixels, int *filtered, int heigh, int width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

	for (x = 0; x < width; x+=4) {
		for (y = 0; y < heigh; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++){
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
          if (((x+1+dx-2) >= 0) && ((x+1+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
  						sum += pixels[(x+1+dx-2),(y+dy-2)] * filter[dx][dy];

          if (((x+2+dx-2) >= 0) && ((x+2+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
              sum += pixels[(x+2+dx-2),(y+dy-2)] * filter[dx][dy];

          if (((x+3+dx-2) >= 0) && ((x+3+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
              sum += pixels[(x+3+dx-2),(y+dy-2)] * filter[dx][dy];
              
          }
    	filtered[x*heigh+y] = (int) sum/273;
        filtered[(x+1)*heigh+y] = (int) sum/273;
        filtered[(x+2)*heigh+y] = (int) sum/273;
        filtered[(x+3)*heigh+y] = (int) sum/273;
		}
	}
}
```
    
    Teniendo en cuenta que el video tiene un ancho de 1920 por defecto y es divisible este por 4, he probado a optimizarlo mediante el desenrollado de bucles.
    Con esto mejoramos el tiempo tanto en secuencial como en paralelo, si utilizamos en el script "pract3bench.sh" para hallar los tiempos de respuesta de cada una
    de las 10 ejecuciones y con esto hallamos el tiempo medio de respuesta, vemos que los resultado obtenidos son que para el caso del secuencial 
    el tiempo medio de respuesta es de 2.0370117  y para el paralelo de 1.2225586.
    
    Si tenemos en cuenta que para el caso secuencial antes de esta optimizacion teniamos un tiempo medio de respuesta de 2.2351563 y ahora de 2.0370117 tenemos
    una ganancia de 1.097272195 (Con respecto al caso secuencial base) gracias a la mejora
    
    Si lo comprabamos con el caso paralelo antes teniamos una tiempo medio de respuesta de 1.3195315 y con la optimizacion de 1.2225586 por lo que tenemos
    una ganancia de  1.079319633 (Con respecto al caso paralelo anterior).
    
    Vemos que las ganacias son muy pequeñas con esta "optimizacion" pero se mejora ambos caso base
    
    Si utilizamos OpenMP en fgauss de la siguiente manera:

*Segunda Optimización*    
```
void fgauss (int *pixels, int *filtered, int heigh, int width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;
  #pragma omp parallel for private(x,y,sum,dx,dy) shared(pixels,filtered,heigh, width,filter)
	for (x = 0; x < width; x++) {
		for (y = 0; y < heigh; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++){
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
          
                }
			filtered[x*heigh+y] = (int) sum/273;
    	
		}
	}
}
```
    El resultado obtenido es que para el caso el cual ya habia una paralelizacion realizada en al main, esta optimizacion afecta negativamente al tiempo de respuesta, esto es debido a que estas generando tareas que consisten en aplicar un filtro a una imagen y
    repartiendo estas tareas entre varios threads y a su vez en cada tarea estas repartiendo las iteraciones del primer bucle entre los distintos threads. El tiempo de respuesta seria de 1.4980469
    El secuencial pasaria a ser paralelo solo que esta vez su region paralela se encuentra en fgauss y obtendria un tiempo medio de respuesta de 1.1720703 donde tendriamos
    una mejora muy aceptable, por lo que de esto concluimos que si no tenemos paralelizado el codigo principal, es mejor paralelizar el fgauss mediante OpenMP
    Ya que paralelizando la main mediante tareas teniamos un tiempo medio de respuesta de 1.3195315. 
    
Si aplicamos ambas optimizaciones:
```
void fgauss (int *pixels, int *filtered, int heigh, int width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;
  #pragma omp parallel for private(x,y,sum,dx,dy) shared(pixels,filtered,heigh, width,filter)
	for (x = 0; x < width; x+=4) {
		for (y = 0; y < heigh; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++){
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
          if (((x+1+dx-2) >= 0) && ((x+1+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
  						sum += pixels[(x+1+dx-2),(y+dy-2)] * filter[dx][dy];

          if (((x+2+dx-2) >= 0) && ((x+2+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
              sum += pixels[(x+2+dx-2),(y+dy-2)] * filter[dx][dy];

          if (((x+3+dx-2) >= 0) && ((x+3+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
              sum += pixels[(x+3+dx-2),(y+dy-2)] * filter[dx][dy];
          }
			filtered[x*heigh+y] = (int) sum/273;
    	filtered[(x+1)*heigh+y] = (int) sum/273;
      filtered[(x+2)*heigh+y] = (int) sum/273;
      filtered[(x+3)*heigh+y] = (int) sum/273;
		}
	}
}

```
    Obtenemos que para el caso que no tiene paralelizado la main, es decir que originalmente es secuencial, obtenemos un tiempo de respuesta medio de 0.9979492
    y una ganacia respecto al caso secuencial base (sin optimizaciones) de 2.239749578
    
    En el caso de la version paralela anterior con las dos optimizaciones aplicadas.
    Tenemos un tiempo medio de respuesta de 1.399707 bastante peor que en el caso paralelo original
    
    
    