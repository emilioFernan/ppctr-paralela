#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

void fgauss (int *, int *, int, int);

int main(int argc, char *argv[]) {



   FILE *in;
   FILE *out;
   int i, j, size, seq = 8;
   int **pixels, **filtered;

   if (argc == 2) seq = atoi (argv[1]);

//   chdir("/tmp");
   in = fopen("movie.in", "rb");
   if (in == NULL) {
      perror("movie.in");
      exit(EXIT_FAILURE);
   }

   out = fopen("movie.out", "wb");
   if (out == NULL) {
      perror("movie.out");
      exit(EXIT_FAILURE);
   }

   int width, height;

   fread(&width, sizeof(width), 1, in);
   fread(&height, sizeof(height), 1, in);

   fwrite(&width, sizeof(width), 1, out);
   fwrite(&height, sizeof(height), 1, out);

   pixels = (int **) malloc (seq * sizeof (int *));
   filtered = (int **) malloc (seq * sizeof (int *));
   float inicio=omp_get_wtime();
   #pragma omp parallel private(i,size) shared(pixels,filtered,width,height,seq)
   {
   for (i=0; i<seq; i++)
   {
      pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
      filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
   }


    i = 0;
   #pragma  omp single
   {


   do
   {

      size = fread(pixels[i%seq], (height+2) * (width+2) * sizeof(int), 1, in);

      if (size)
      {

         #pragma omp task
         fgauss (pixels[i%seq], filtered[i%seq], height, width);





      }
      i++;
      if(i%seq==0){

        #pragma omp taskwait
        for(int p=0;p<seq;p++){

        fwrite(filtered[p%seq], (height+2) * (width + 2) * sizeof(int), 1, out);
        }
        i=0;
      }

   } while (!feof(in));

   #pragma omp taskwait
   for(int p=0;p<i-1;p++){


    fwrite(filtered[p%seq], (height+2) * (width + 2) * sizeof(int), 1, out);
    }

  }

  }
float segundos=omp_get_wtime();
   for (i=0; i<seq; i++)
   {
      free (pixels[i]);
      free (filtered[i]);
   }
   free(pixels);
   free(filtered);

   fclose(out);
   fclose(in);


  printf("%f\n",(segundos-inicio));


//   chdir("/tmp");
   in = fopen("movie.in", "rb");
   if (in == NULL) {
      perror("movie.in");
      exit(EXIT_FAILURE);
   }

   out = fopen("movie2.out", "wb");
   if (out == NULL) {
      perror("movie.out");
      exit(EXIT_FAILURE);
   }



   fread(&width, sizeof(width), 1, in);
   fread(&height, sizeof(height), 1, in);

   fwrite(&width, sizeof(width), 1, out);
   fwrite(&height, sizeof(height), 1, out);

   pixels = (int **) malloc (seq * sizeof (int *));
   filtered = (int **) malloc (seq * sizeof (int *));

   for (i=0; i<seq; i++)
   {
      pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
      filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
   }

   float inicioSec=omp_get_wtime();

    i = 0;



   do
   {

      size = fread(pixels[i%seq], (height+2) * (width+2) * sizeof(int), 1, in);

      if (size)
      {


         fgauss (pixels[i%seq], filtered[i%seq], height, width);
         fwrite(filtered[i%seq], (height+2) * (width + 2) * sizeof(int), 1, out);




      }
      i++;

   } while (!feof(in));

   float segundosSec=omp_get_wtime();




   for (i=0; i<seq; i++)
   {
      free (pixels[i]);
      free (filtered[i]);
   }
   free(pixels);
   free(filtered);

   fclose(out);
   fclose(in);


  printf("%f\n",(segundosSec-inicioSec));
   return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, int heigh, int width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

	for (x = 0; x < width; x++) {
		for (y = 0; y < heigh; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++){
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];

          }
			filtered[x*heigh+y] = (int) sum/273;

		}
	}
}
