#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <sys/time.h>
#define DEBUG false
#define FEATURE_LOGGER true
#define FEATURE_OPTIMIZE true
void logger(int numeroThreads,std::condition_variable*,char *,long *,double * );
void sumaArray(double * array,int tamanho, int orden);
void xorArray(double * array,int finalPos,int ordenHilo);
void sumaNoLogger(double * array,int finalPos,int ordenHilo,double* suma  ,int* finalizacion);
void xorNoLogger(double * array,int finalPos,int ordenHilo,long * valor  ,int * finalizacion);
std::mutex g_m;
std::condition_variable g_c;
int ordenHiloActual;//variable donde se pasara al Logger el orden de cada hilo
double sumaParcial;//variable donde se pasara al Logger la suma de cada hilo
bool ready=false;//variable que usare para que los hilos vayan de uno a uno con el Logger y no varios hilos seguidos
long xorParcial;
struct timeval ti,tf;
std::atomic<int>numFinalizacion(0);
int main (int argc,char** argv){
  gettimeofday(&ti,NULL);


  if(argc<2){
    //Parametros Incorrectos

    fprintf(stderr, "%s\n","No ha introducido el numero de parametros correcto" );
    return 0;
  }



  double segundos;
  int tamanho=atoi(argv[1]);


  double *array;

  array=(double *)malloc(tamanho * sizeof(double));

  for(int i=0;i<tamanho;i++){
    array[i]=(double)i;
  }

  long xorMain;

  char *operacion=argv[2];

  double sumaMain;

  const char *sumaPuntero="sum";

  if(argc==3){
    //No se ha metido el flag --multithread por tanto se ejecutara Secuencialmente
    if(DEBUG)
    printf("%s\n","Programa Secuencial" );

    if(strcmp(operacion,sumaPuntero)==0){
      for(int i=0;i<tamanho;i++){

        sumaMain+=array[i];

      }
    }
    else{
      xorMain=((long)array[0]);
      for(int i=1;i<tamanho;i++){

        xorMain=xorMain^((long)array[i]);


      }

    }

    gettimeofday(&tf,NULL);

    segundos=(double)(tf.tv_sec-ti.tv_sec)+(double)(tf.tv_usec-ti.tv_usec)/1000000;
    printf("%g\n",segundos);
    return 1;

  }
  else{
  //Caso Paralelo
  int numeroThreads=atoi(argv[4]);




  std::condition_variable g_c2;
  std::thread threadLog;
  double sumaLogger;
  long xorLogger;


  std::thread threads[numeroThreads];
  int tamanhoRepartido=tamanho/numeroThreads;
  int tamanhoExtraPrimero=tamanho-(tamanhoRepartido*numeroThreads);



  int inicio=tamanhoRepartido+tamanhoExtraPrimero;

  double *puntero;

  if(FEATURE_LOGGER){


    //Creacion de hilo Logger

    std::unique_lock<std::mutex> lock(g_m);
    threadLog=std::thread(logger,numeroThreads,&g_c2,operacion,&xorLogger,&sumaLogger);

    if(strcmp(operacion,sumaPuntero)==0){
      //Realizamos la suma
      for(int i=0;i<numeroThreads;i++){
            if(i==0){
              puntero=&array[0];
              threads[i]=std::thread(sumaArray,puntero,tamanhoRepartido+tamanhoExtraPrimero,i);
            }
            else {
                puntero=&array[inicio];
                threads[i]=std::thread(sumaArray,puntero,tamanhoRepartido,i);
                inicio+=tamanhoRepartido;
          }
      }


      for(int i=0;i<tamanho;i++){

        sumaMain+=array[i];

      }


    }
    else{
      //Realizamos la xor
      for(int i=0;i<numeroThreads;i++){
            if(i==0){
              puntero=&array[0];
              threads[i]=std::thread(xorArray,puntero,tamanhoRepartido+tamanhoExtraPrimero,i);
            }
            else {
                puntero=&array[inicio];
                threads[i]=std::thread(xorArray,puntero,tamanhoRepartido,i);
                inicio+=tamanhoRepartido;
          }
      }

      xorMain=((long)array[0]);
      for(int i=1;i<tamanho;i++){

        xorMain=xorMain^((long)array[i]);


      }
    }



    g_c2.wait(lock);

    for(int i=0;i<numeroThreads;i++){
      threads[i].join();
    }
    threadLog.join();



    free(array);



    segundos=(double)(tf.tv_sec-ti.tv_sec)+(double)(tf.tv_usec-ti.tv_usec)/1000000;

    if(sumaLogger==sumaMain){


      printf("Soy el main y sume esto: %f \n",sumaMain);
      printf("Esto ha sumado el Logger: %f \n",sumaLogger);

        printf("Coincide la suma\n");
        printf("%g\n",segundos);
      return 1;
    }
    if(xorLogger==xorMain){


      printf("Soy el main y la xor me dio esto: %ld  \n",xorMain);
      printf("Esto ha xoreado el Logger: %ld  \n",xorLogger);

        printf("Coincide la xor\n");
        printf("%g\n",segundos);
      return 1;
    }


    printf("No Coincide¡ERROR!\n");
    return 0;
  }
  else{
    //El programa tiene desactivado el logger y la main se hara cargo de realizar la lectura de los hilos y pintar el registro de finalizacion
    if(DEBUG)
      puts("El programa tiene desactivado el Logger");
      int * arrayFinalizacion= (int*)malloc(numeroThreads * sizeof(int));
      double sumaHilos;
      long valor=0;
      if(strcmp(operacion,sumaPuntero)==0){
        for(int i=0;i<numeroThreads;i++){
              if(i==0){
                puntero=&array[0];
                threads[i]=std::thread(sumaNoLogger,puntero,tamanhoRepartido+tamanhoExtraPrimero,i,&sumaHilos,arrayFinalizacion);
              }
              else {
                  puntero=&array[inicio];
                  threads[i]=std::thread(sumaNoLogger,puntero,tamanhoRepartido,i,&sumaHilos,arrayFinalizacion);
                  inicio+=tamanhoRepartido;
            }


            }
            for(int i=0;i<tamanho;i++){

              sumaMain+=array[i];

            }
          }
        else{
          for(int i=0;i<numeroThreads;i++){
                if(i==0){
                  puntero=&array[0];
                  threads[i]=std::thread(xorNoLogger,puntero,tamanhoRepartido+tamanhoExtraPrimero,i,&valor,arrayFinalizacion);
                }
                else {
                    puntero=&array[inicio];
                    threads[i]=std::thread(xorNoLogger,puntero,tamanhoRepartido,i,&valor,arrayFinalizacion);
                    inicio+=tamanhoRepartido;
              }

          }
          xorMain=((long)array[0]);
          for(int i=1;i<tamanho;i++){

            xorMain=xorMain^((long)array[i]);


          }
        }


      for(int i=0;i<numeroThreads;i++){
        threads[i].join();
      }
        gettimeofday(&tf,NULL);


      for(int i=0;i<numeroThreads;i++){
        printf("El hilo numero: %d ha finalizado el %d\n",arrayFinalizacion[i],i+1);
      }
          free(array);


      segundos=(double)(tf.tv_sec-ti.tv_sec)+(double)(tf.tv_usec-ti.tv_usec)/1000000;

      if(sumaHilos==sumaMain){


        printf("Soy el main y sume esto: %f \n",sumaMain);
        printf("Esto han sumado los hilos: %f \n",sumaHilos);

        printf("Coincide la suma\n");

        printf("%g\n",segundos);
        return 1;
      }

      if(valor==xorMain){


        printf("Soy el main y mi xor: %ld \n",xorMain);


        printf("Esto han xoreado los hilos: %ld \n",valor);


        printf("Coincide la xor\n");

        printf("%g\n",segundos);
        return 1;
      }
  }

  }
}
/*
Funcion donde los hilos realizaran la suma en caso de que el Logger este activado
*/
void sumaArray(double * array,int finalPos,int ordenHilo){

  std::unique_lock<std::mutex> lock(g_m);

  g_c.wait(lock,[]{return ready;});


  double valorSuma=0;
  for(int i=0;i<finalPos;i++){
    valorSuma+=array[i];
  }


  ordenHiloActual=ordenHilo;
  sumaParcial=valorSuma;
  ready=false; //A false el logger es el unico que puede desbloquearse
  g_c.notify_all();//Despierto al logger

  lock.unlock();

}
/*
Funcion donde los hilos haran la xor en caso de que el Logger este activado
*/
void xorArray(double * array,int finalPos,int ordenHilo){
  std::unique_lock<std::mutex> lock(g_m);

  g_c.wait(lock,[]{return ready;});




  long valorXor=(long)array[0];

  for(int i=1;i<finalPos;i++){

    valorXor=valorXor^(long)array[i];


  }

  ordenHiloActual=ordenHilo;
  xorParcial=valorXor;
  ready=false;
  g_c.notify_all();

  lock.unlock();
}

void logger(int numeroThreads,std::condition_variable* g_c2, char* tipo,long* xorLogger,double* sumaLogger){
  if(DEBUG)
  printf("Soy el logger\n");

  std::unique_lock<std::mutex> lock(g_m);
  int* ordenDeFinalizacion=(int *)malloc(numeroThreads * sizeof(int));
  if(DEBUG)
    printf("tipo es:%s\n",tipo);
    double sumaTotal;
    long xorTotal;
  if(strcmp(tipo,"sum")==0){
    double* resultadosParciales=(double *)malloc(numeroThreads * sizeof(double));

  for(int i=0;i<numeroThreads;i++){

    if(DEBUG)
    printf("Entro en el wait\n");
    ready=true;
    g_c.notify_one();
    g_c.wait(lock);
    if(DEBUG)
    printf("He salido del wait\n");
    resultadosParciales[ordenHiloActual]=sumaParcial;
    ordenDeFinalizacion[i]=ordenHiloActual;

  }


  for(int i=0;i<numeroThreads;i++){

    sumaTotal+=resultadosParciales[i];

  }
    gettimeofday(&tf,NULL);
  free(resultadosParciales);
  sumaLogger[0]=sumaTotal;

}
else{
      long* resultadosParciales=(long *)malloc(numeroThreads * sizeof(long));
      for(int i=0;i<numeroThreads;i++){
        if(DEBUG)
        printf("Entro en el wait\n");
        ready=true;
        g_c.notify_one();
        g_c.wait(lock);
        if(DEBUG)
        printf("He salido del wait\n");
        resultadosParciales[ordenHiloActual]=xorParcial;
        ordenDeFinalizacion[i]=ordenHiloActual;

      }

      long xorTotal=resultadosParciales[0];
      if(DEBUG)
      printf("El hilo numero: %d ha retonardo: %ld\n",0,resultadosParciales[0]);
      for(int i=1;i<numeroThreads;i++){
        if(DEBUG)
        printf("El hilo numero: %d ha retonardo: %ld\n",i,resultadosParciales[i]);
        xorTotal=xorTotal^resultadosParciales[i];
      }
      gettimeofday(&tf,NULL);

      free(resultadosParciales);
        if(DEBUG)
        printf("xorTotal va valiendo: %ld\n",xorTotal);
        xorLogger[0]=xorTotal;
  }




  printf("REGISTRO DE FINALIZACION\n");


  for(int i=0;i<numeroThreads;i++){
    printf("El hilo numero: %d ha finalizado el %d\n",ordenDeFinalizacion[i],i+1);
  }

    free(ordenDeFinalizacion);

    lock.unlock();
    g_c2[0].notify_one();





}
/*
  Funcion que hara la suma en caso de el Logger este desactivado
*/
void sumaNoLogger(double * array,int finalPos,int ordenHilo,double* suma  ,int * finalizacion){

  double valorSuma=0;
  for(int i=0;i<finalPos;i++){
    valorSuma+=array[i];
  }
  suma[0]=suma[0]+valorSuma;
  finalizacion[numFinalizacion]=ordenHilo;
  numFinalizacion++;


}
/*
  Funcion que hara la xor en caso de el Logger este desactivado
*/
void xorNoLogger(double * array,int finalPos,int ordenHilo,long* valor  ,int * finalizacion){
  long valorXor=(long)array[0];

  for(int i=1;i<finalPos;i++){

    valorXor=valorXor^(long)array[i];
if(DEBUG)
    printf("valor xor: %ld\n",valorXor);

  }


    valor[0]=valor[0]^valorXor;
    finalizacion[numFinalizacion]=ordenHilo;
    numFinalizacion++;


}
