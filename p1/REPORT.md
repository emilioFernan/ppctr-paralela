# P1: Multithreading en C++
## Preguntas
### 1.Indica el tiempo que has necesitado para realizar la práctica y el tiempo invertido en cada una de las secciones de la rúbrica. [Respuesta: 6 líneas]
    Para realizar la práctica me ha llevado del orden de 18 horas solamente de codigo sin contar la elaboracion de la memoria, todo en general unas 25 horas
    Reduccion paralela -> 3 horas
    Correcto particionado y explicacion de variaciones -> 1 horas
    Resultados, analisis y evaluacion de rendimientos -> 2,5 horas
    Procesamiento de argumentos y establecimiento de configuracion del programa -> 2,5 horas
    ROI y correcta medicion del tiempo -> 0,5 horas
    Hilo Logger -> 5 horas
    Conector con java -> 1,5 
    Registro de finalizacion -> 1 hora
    Mantenibilidad,Modularidad, Refactorizacion -> 1 hora
    
    
### 2.Indica qué has conseguido y qué te falta por cada apartado de la rúbrica. Además, describe las dificultades encontradas durante dicho proceso. [1-20 líneas por sección]
    He conseguido basicamente realizar la reduccion paralela, el hilo logger y el registro de finalizacion principalmente.
    Me encontre varias dificultades de cara a la programacion en C debido a que estaba algo "oxidado" con este lenguaje por lo que referente a C, esta asignatura me ha venido de perlas
    Otro problema era que a veces me sentia perdido con el guión, en el sentido de pensar, ¿Esto como lo puedo hacer? o no saber exactamente lo que me estan pidiendo
    que posiblemente es un problema de compresion mio mas que de la elaboracion del guion pero es entendible ya que, esta accion de reflexionar y pensar en como puedo realizar cierta tarea u objetivo
    es parte de la dinamica de aprender y entender el paralelismo como solucion.
    En referente a la sincronizacion en C era la primera vez que utilizo estos mecanismos en C++ que practicamente es igual, lo unico cosas como por ejemplo la variables atomicas con el fin de optimizar el paralelismo
    no fui capaz de comprender como usarlas u/o aplicarlas a mi codigo.
    El conector Java fui incapaz de hallar una forma siquiera de empezar, estaba perdido con ese tema y cuando tenia ratos libres si que intentaba buscar cosas pero una vez se me fueron acumulando las practicas y al ser como un bonus decidi
    dejarlo de lado.
    
    La Mantenibilidad si que he intentado hacerla lo mejor posible, el como se afronta el programa ante fallos a la hora de meter los parametros, aparte he añadido comentarios con el fin
    de hacer mas sencillo entender el codigo como el modificarlo o mantenerlo.
    
### 3.Explica el funcionamiento del programa con mayor detalle que lo indicado en el enunciado, haciendo especial hincapié en el paralelismo y las sincronizaciones.[5-40 líneas]
    El programa que he realizado en caso de que se ejecute sin el flag --multithread se ejecutara de manera secuencial, es decir simplemente hara la suma recorriendo el array
    Si usamos el flag --multithread se ejecutara de manera paralela, es decir, primero creara los diferentes hilos tantos como hayas determinado en el parametro 
    los hilos creados haran una operacion u otra en funcion del parametro que hayamos pasado por consola.
    Si tenemos el flag FEATURE_LOGGER desactivado, los hilos iran sumando sus partes y acumulandolas en una variable globlal dependiente del tipo de operacion
    Con esta variable no hay problemas de sincronizacion ya que solo es de lectura. Al estar el Logger desactivado, es la main la que se va a encargar de imprimir por pantalla
    el registro de finalizacion de los hilos. Este registro se determina por un array en el cual se iran poniendo el orden de cada hilo segun vayan acabando.
    Para conseguir esto hacen uso de una variable atomica la cual determina el orden en el que acaba cada hilo (casilla del array donde va a escribir su numero el thread)
    Como es de lectura y escritura es necesario que sea variable atomica para la exclusion mutua.
    Si tenemos activado el Logger, el es el encargado de ir recibiendo las sumas de cada thread y una vez esten todas avisar a la main que estara bloqueada esperando a que el Logger junte las operaciones parciales 
    de los threads.
    Por tanto necesitaremos elementos de sincronizacion los cuales, en el caso de que este desactivado, la sincronizacion se hace mediante el metodo wait, el hilo principal espera a los hijos
    en este caso como se nos pide sincronizar el logger con la main mediante una variable condicional sera necerario crearla. Otra variable condicional sera necesaria para la sincronizacion
    ente Logger y los hilos, ya que estos ultimos deberan despertar al Logger una vez tengan su resultado calculado, por lo que la dinamica seria algo como, un thread realiza sus operaciones
    despierta al Logger, este hace sus operaciones con el resultado y se vuelve a bloquear en la variable condicional hasta que otro hilo lo despierte.
    Una vez acaben los thread, el Logger despertara al hilo principal el cual hara la comparacion entre el resultado del Logger y el calculado por el.
    
    Para evitar que un hilo despierta a otro hilo es necesario asociar a una variable condicional, una variable boolean que determine si le toca al Logger o a un hilo
    de manera que vayan de uno a uno los hilos y  no se pisen entre ellos, ya que estos escriben en variables globales su orden y el valor que han calculado
    para que el Logger puede juntar los resultados y realizar el registro de finalizacion.
    
    La variable condicional que sincroniza Logger con la main es necesario pasarla como parametro ya que esta no es global



### 4.(Base) Explica el particionado aplicado, qué variaciones se te ocurren y posibles efectos. [5-10 líneas]
    El particionado del array se hace de manera equitativa entre el numero de threads siempre que se pueda, de manera que si no se puede particionar de forma exacta 
    se asignara la carga extra al primer thread. Para lograr esto primero calculo el trabajo de cada thread mediante la division entre el tamaño del array de datos y el numero de threads
    despues multiplicando el trabajo de cada thread por el numero de threads y esto se lo resto al tamaño del array para calcular de esta manera la carga extra que tiene que hacer el primer thread.
    A la hora de dar la carga a cada thread, lo que hago es pasarle un puntero de la posicion donde comienza en el array y les paso un numero para determinar hasta donde tiene que operar cada thread.
    En el caso del primer thread ese numero sera la carga repartida equitativamente + carga extra ya que en caso de no ser exacto tendra que realizar mas sumas.
    Una variacion podria ser que la carga extra en vez de darsela toda al primer thread, tambien repartirla entre los threads, es decir que si el thread uno tiene que hacer 2 sumas de mas, que el haga una y el siguiente thread la otra
    Esto mejoraria con creces el rendimiento ya que los threads no estaran en modo ocioso mientras el primer thread realiza esa carga extra.
    Otra opción que veo es la de mediante una variable global que indique por que casilla van del array y otra que guarde la suma total que los hilos vayan sumando sin asignarles ningun intervalo del array
    si no que ellos vayan cogiendo el valor de la casilla actual,lo sumen a la variable global y aumenten el indice de la casilla.
    Con esto te evitarias esa perdida de tiempo en la que el thread uno hace la carga extra pero tienes que tener cuidado con el acceso a esta variables ya que tendra que preservar la exclusion mutua
    
    
### 5.(Base) Explica cómo has hecho los benchmarks, qué metodología de medición has usado (ROI+gettimeofday, externo), proporciona los resultados (recomendable gráfica) y haz un análisis de los mismos. [5-30 líneas de análisis]
```
#!/usr/bin/env bash
file=pract1.log
if [ -f $file ];then
    rm pract1.log
fi
touch $file

g++ -std=c++11 -lm -pthread -o pract1 pract1.c

nthreads=3
for tamano in  10 100 1000 10000 10000000 100000000; do



  echo Tamaño matriz: $tamano >> $file
    for operacion in xor sum; do
        #Ejecuciones WarmUp
      ./pract1 $tamano $operacion  1>/dev/null
      ./pract1 $tamano $operacion  1>/dev/null
      ./pract1 $tamano $operacion  1>/dev/null
      ./pract1 $tamano $operacion  1>/dev/null
      echo $operacion >> $file
      for i in 1 2 3 4 5; do

        echo "Secuencial:">> $file
        ./pract1 $tamano $operacion >> $file
        echo "Paralelo:">> $file
        ./pract1 $tamano $operacion --multithread $nthreads >> $file
      done

    done
done
    
```

De los tiempos de respuesta sacados del script, he realizado una grafica con los tiempos medio de respuesta y otra en la que se refleja el comportamiento conforme al Speedup

![](images/Tiempo_medio_de_respuesta_Xor.png)

![](images/Tiempo_medio_de_respuesta_Sum.png)

![](images/SpeedUpDef.png)

    En estas graficas los casos realizados en paralelo han sido mediante 3 threads y con el Logger activado
    
    Como vemos en el caso de mi implementacion el caso paralelo es peor que el secuencial, esto puede ser debido a que como esta implementado el Logger ya que añade esperas que pueden empeorar este tiempo.
    Que el tiempo de respuesta sea peor en el caso del paralelo que en el del secuencial es posible, sobretodo cuando el array que tiene que sumar el problema es pequeño, ya que tanto la sincronizacion como la creacion 
    de los hilos conllevan un gasto en tiempo por el cual, si el tamaño del problema es muy pequeño el tiempo en caso secuencial deberia ser menor que el paralelo
    Y lo que deberiamos ir viendo es que a medida que el tamaño aumenta, llegara un punto en el que el programa en paralelo tiene un mejor tiempo.
    
    En las graficas que he generado lo que se puede ver es que tanto el tiempo como el SpeedUp aumenta conforme al tamaño del problema. Cuanto mayor es el problema mayor es el tiempo de respuesta
    y mayor el SpeedUp, eso es un comportamiento logico y esta bien. 
    
    Debido a que los tiempos eran muy pequeños para tamaños menores de 1000, decidi pasar directamente a un tamaño 10000000 para comprobar el comportamiento ante problemas grandes
    
    Para un tamaño tan grande el caso del paralelo deberia ser mejor que el secuencial pero como se observa en las graficas no es asi, por lo que tiene que ser debido a mi implementacion del codigo.

### 6.(Mantenibilidad) Explica qué has refactorizado y cómo ha mejorado la mantenibilidad. [2-20 líneas]

    Para mejorar la mantenibilidad lo que he realizado es una comprobacion del numero de parametros ya que en funcion de ello, el programa hara una cosa u otra
    Ademas compruebo que los parametros introducidos son correctos para iniciar la operacion.
    Tambien he añadido comentarios con el fin de hacer mas facil mantener y mejorar el codigo.
    Se muestran mensajes de error en caso de que los parametros introducidos no sean los correctos.
    Con el modo DEBUG activado te saldran mensajes en los que te muestran que devuelve cada thread, el resultado que ha calculado el Logger, como se va realizando 
    la sincronizacion(El logger y los hilos avisa antes de bloquearse y cuando se desbloquean unos a otros).
    
    