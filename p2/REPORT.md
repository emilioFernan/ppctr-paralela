# P2: Paralelización de un Programa de Multiplicación de Matrices Triangulares

Emilio Fernández López

18/12/2018

## Ejercicio 1


1.Explica brevemente la función que desarrolla cada una de las directivas y funciones
    de OpenMP que has utilizado.
        
        #pragma omp parallel-> Define una región paralela, al llegar a esta directiva se crea un grupo de threads
                               Hay una barrera implicita al final de esta región donde se destruyen los hilos creados.
                               
        #pragma omp for-> Reparte las iteraciones de un bucle entre los diferentes threads de una región paralela
        
        omp_get_max_threads()-> Función que devuelve el maximo numero de threads que pueden ser usados en el sistema
        
        num_threads() -> Esta clausula indica el numero de threads que se ejecutaran en el bloque definido por la región paralela, es decir el numero de threads 
                         que encontraremos en una región paralela en la que hayamos usado dicha clausula
                         
        collapse() -> Permite paralelizar bucles anidados, ya que la directiva #pragma omp for solo paralelizaria las iteraciones del bucle mas externo
        
        
2.Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta.
    
"#pragma omp parallel private(i,j,k) shared(A,B,C,dim)""
    
    He puesto como private aquellas variables iterativas como son la i, la j o la k debido a que estas se definen dentro de la región paralela
    
    El resto las he puesto como shared ya que se pasan como parametro y tiene un valor asignado fuera de la región paralela por lo que hay solo una copia de estas en memoria.
    
    
3.Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.
     
     Una vez se llega a una directiva #pragma omp for, los hilos se reparten el trabajo, es decir, la iteraciones de un bucle.
     Estos se reparten las iteraciones de manera equitativa siempre que se pueda. De una forma analoga a la practica 1 donde cada hilo sumaba "X" elementos del array
     que se repartian previamente de manera equitativa según el tamaño del array.
     Para explicarlo con un ejemplo. Tenemos una matriz cuadrada de orden 10 y la multiplicamos por otra de orden 10.
     La multiplicacion de matrices se hace multiplicando los elementos de una fila por los elementos de una columna de la otra matriz y sumando estos productos.
     El bucle que nosotros repartimos, es el bucle que recorre las filas. Con lo que al repartir el trabajo estamos haciendo cada hilo calcule
     una fila de la matriz resultante por cada iteracion que este haga.
     
     
4.Calcule la ganacia (speedup) obtenido con la paralelización.

        He usado el siguiente benchmark para hallar los tiempos de ejecucion de matmul para secuencial y paralalelo probando con diferentes tamaños. 
        Para ello primero compilo una version del codigo secuencial a la que llamo pract2SEC y depues otra versión pero empleando OpenMP a la que denomino prac2OMP
        El orden de la matriz lo voy aumentando cada iteración 160 y esto lo hago un total de 5 veces.
        Por cada tamaño hago dos ejecuciones warm up para cada caso y acto seguido ejecuto 10 veces cada caso y los tiempos los voy guardando en un fichero "results1.log"
        Despues con ese fichero para sacar el tiempo hago la media de las 10 medidas para cada tamaño y para cada caso.
    
        Aunque pone que hay que trabajar con matrices de 1000x1000 como pruebo 5 tamaños y por cada unos de ellos hago 20 ejecuciones en total he decidido
        usar como tope 660 por el simple hecho de ahorrar tiempo ya que el comportamiento será homogeneo con respecto al orden de la matriz

**benchmarkingp2.sh:**

```sh
#!/usr/bin/env bash
file=results1.log
touch $file

tamanho=20
for season in 1 2 3 4 5 ; do
  echo Ejecuciones Warmup

  ./pract2OMP $tamanho
  ./pract2SEC $tamanho
  ./pract2OMP $tamanho
  ./pract2SEC $tamanho

  echo Tamaño matriz: $tamanho
    for benchcase in OMP SEC; do
        echo pract2$benchcase >> $file
        for i in `seq 1 1 10`;
        do
          printf "$i:" >> $file # iteration

            ./pract2$benchcase $tamanho>> $file # results dumped

            sleep 1
        done

    done
  tamanho=$(($tamanho+160))
done
```

![](images/Tiempo_de_ejecución_Matmul__1_.png)


    En la grafica podemos observar el tiempo de ejecución del metodo matmul tanto en paralelo como en sencuencial para diferentes tamaños de matriz
    Como se puede observar para matrices cuyo orden es muy pequeño el tiempo ejecución es menor para el caso del secuencial.
    Esto es debido a que el tiempo que le conlleva realizar la operacion es muy pequeño y el añadir un tiempo para la creacion y gestion de los hilos lo unico que consigue
    es empeorar el tiempo.
    Pero si hablamos de matrices cuyo orden es superior a 100 ya podremos ver que en el caso del paralelo tenemos un mejora respecto al tiempo y si vamos aumentando el orden llegará
    un momento en el cual el tiempo del paralelo sea la mitad con respecto al secuencial.
    Por lo que si nos fijamos en el speedup hallado, veremos que el comportamiento habitual es que cuanto mayor sea el orden de la matriz mayor sera el speedup.


![](images/Speedup_Matmul.png)

    Vemos que el speedup alcanza el punto más alto para el mayor orden de matriz que empleamos
    
    
## Ejercicio 2
    
¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la clausula schedule? Explica con detalle a qué se debe esta diferencia.
Ejecutando varias veces cada programa previamente (WarmpUp) finalmente el tiempo del ejercicio 1 ha sido de 0.576172 y el del ejercicio 2 ha sido de 0.457031 
Por tanto tenemos una ganancia de 1,16
    
    Esta diferencia viene dada por la manera de que tiene cada metodo de realizar la multiplicacion.
     Matmul realiza la multiplicacion de la manera clasica, es decir fila por columna, esto conlleva que en caso de tener que una matriz triangular como es el caso
     muchas de las operaciones que realizamos son multiplicaciones por 0.
     matmul_sup hace en este caso una multiplicacion pero especial para matrices triangulares con el fin de no hacer esas multiplicaciones por 0 y ahorrarse operaciones.
     Este ahorro de operaciones hace que tarde menos tiempo y por tanto tengamos una ganancia.
     
     
¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las
operaciones que tiene que realizar cada thread.
    
    Haciendo diferentes pruebas he observado que el que mejor resultado obtiene es para el caso del dynamic, esto puede ser debido a que reparte mejor la carga de trabajo ya que static usa tamaños fijos de paquete
    y guided incluye mas overhead y además empieza con paquetes grandes. El dynamic da mejores resultado aparte de porque habra multiplicaciones que conlleven menos carga de trabajo que otras y por tanto
    estas irregularidades se solventan mediante este algoritmo de la forma más eficiente.
    
Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función de cómo se reparte la carga de trabajo

    Desde mi punto de mi vista el tamaño optimo debería ser para repartir los paquetes de forma equitativa es decir en funcion del numero threads, que estos
    o procesen un numero igualatorio de paquetes. Siempre que esto se puede ya que en algunos casos no se podra repartir de forma equitativa y algunos tendran carga extra de trabajo

Para el algoritmo dynamic determina experimentalmente el tamaño del bloqueóptimo. Para ello mide el tiempo de respuesta y speedup obtenidos para al menos
10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados

    Para hallar los diferentes tiempos de respuesta he utilizado el siguiente script:

**dynamic.sh:**

```sh
#!/usr/bin/env bash
file=resultsDynamic.log

touch $file

tamanho=1000


for block_size in 1 10 20 30 40 50 100 200 400 500; do

        echo "Tamaño del Bloque: $block_size" >> $file
        for i in `seq 1 1 10`;
        do
            #Ejecuciones WarmUp

            ./pract2 $tamanho $block_size>> WarmUp
            ./pract2 $tamanho $block_size>> WarmUp
            
            #Ejecucion a estudiar
            ./pract2 $tamanho $block_size>> $file # results dumped

            sleep 1
        done

done
```

Teniendo en cuenta que en el secuencial tenemos un tiempo de respuesta 2.06518575 calculamos tambien el speedup y nos queda la siguiente tabla:

![](images/tablaDynamic.png)


Mediante esta tabla sacamos la respectiva grafica.

![](images/dynamicChart.png)


    Tanto de la tabla como de la gráfica sacamos la siguientes conclusiones, para el algoritmo dynamic si usamos un tamaño de bloque demasiado grande puede ser que incluso 
    consigas un tiempo de respuesta peor que en el caso del secuencial y por lo tanto una ganancia mucho peor.
    Esto puede ser provocado por lo siguiente, si a cada thread le asociamos un bloque demasiado grande vamos a eclipsar la ventaja que ofrece este algoritmo que es la de 
    solapar esas irregularidades que si afectan por ejemplo al static o al guided en caso de que las irregularidades ocurran al principio.
    Por tanto es preferible un tamaño de bloque menor.


Explica los resultados del algoritmo guided, en función del reparto de carga de trabajo que realiza.

    En el ejercicio 2 vemos que la carga de trabajo va de más a menos esto es debido a como esta implementado el método.
    
![](images/forEje2.png)

    como podemos ver en la imagen por cada iteración de "i" tenemos menos iteraciones de "k" que hacer, ya que va en funcion de la "i"
    esto conlleva que tengamos que muchas irregularidades al principio y el algoritmo guided si divide en bloques muy grandes al principio, ocurria que un thread tenga una carga de trabajo muchisimo mayor
    
    
## Ejercicio 3
¿Cómo se comportará? Compáralo con el ejercicio anterior.

    En este caso al igual que en el ejercicio anterior, este es un metodo optimo para la multiplicacion de matrices triangulares este caso la de una inferior por una superior,
    es decir omite aquellas multiplicaciones por 0.
    
    
¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior.

    Al contrario que en el ejercicio anterior, en este problema la carga de trabajo aumenta con cada iteracion por lo que al principio no va a haber muchas irregularidades
    Por lo que el algoritmo guided puede ser el mejor para este caso aunque tampoco creo que el dynamic vaya mal.
    
    
¿Cuál es el peor algoritmo para este caso?
    
    El peor para este caso deberia ser el static ya que repartir en bloques de tamaño constante cuando la carga del problema varia dinamicamente no es nada eficiente  