#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>
#define RAND rand() % 100;

void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim);
void matmul_sup (float *A, float *B, float *C, int dim, int block_size);
void matmul_inf (float *A, float *B, float *C, int dim);
void print_matrix (float *M, int dim); // TODO
void matmulParallel (float *A, float *B, float *C, int dim);
void matmul_sup_sec (float *A, float *B, float *C, int dim ,int block_size);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1;
	int dim=atoi(argv[1]);

	float *A, *B, *C;


  A=(float *)malloc(dim*dim*sizeof(float));
  B=(float *)malloc(dim*dim*sizeof(float));
	C=(float *)malloc(dim*dim*sizeof(float));
	init_mat_sup(dim,A);
//	print_matrix(A,dim);
	init_mat_inf(dim,B);
	//print_matrix(B,dim);




   dim = atoi (argv[1]);
   if (argc == 3) block_size = atoi (argv[2]);

	 float inicio=omp_get_wtime();
	 matmul_sup(A,B,C,dim,block_size);
   float segundos=omp_get_wtime();
/*
	 float inicioSecuencial=omp_get_wtime();
	 	 matmul_sup_secuencial(A,B,C,dim,block_size);
	 float segundosSecuencial=omp_get_wtime();
*/
	free(A);
	free(B);
	free(C);

	//print_matrix(C,dim);
	printf("%f\n",(segundos-inicio));
/*
	printf("Segundos Secuencial: %f\n",(segundosSecuencial-inicioSecuencial));
*/
	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;
	//#pragma omp for private(i,j,m,n,k) shared(dim,M) num_threads(omp_get_max_threads())
	//#pragma omp for
		for (i = 0; i < dim; i++) {
			for (j = 0; j < dim; j++) {
				if (j <= i)
					M[i*dim+j] = 0.0;
				else
					M[i*dim+j] = RAND;
			}
		}

}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;
	//#pragma omp parallel private(i,j,m,n,k) shared(dim,M) num_threads(omp_get_max_threads())
	//#pragma omp for
	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void matmulParallel (float *A, float *B, float *C, int dim)
{

	int i, j, k;

	#pragma omp parallel private(i,j,k) shared(A,B,C) num_threads(omp_get_max_threads())
	{

	#pragma omp for
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++) {
			C[i*dim+j] = 0.0;

		}
	#pragma omp for
	for (i=0; i < dim; i++){

		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)

				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

			}

	}

}
void matmul (float *A, float *B, float *C, int dim)
{

	int i, j, k;




	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++) {
			C[i*dim+j] = 0.0;

		}

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];




}
void matmul_sup (float *A, float *B, float *C, int dim ,int block_size)
{

	int i, j, k;

#pragma omp parallel private(i,j,k) shared(A,B,C,dim) num_threads(omp_get_max_threads())
{

#pragma omp for schedule(static,block_size)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

#pragma omp for schedule(static,block_size)
		for (i=0; i < (dim-1); i++){
			for (j=0; j < (dim-1); j++){
				for (k=(i+1); k < dim; k++){

					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];


				}
			}
		}

}


}
void matmul_sup_sec (float *A, float *B, float *C, int dim ,int block_size)
{

	int i, j, k;


		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		for (i=0; i < (dim-1); i++){
			for (j=0; j < (dim-1); j++){
				for (k=(i+1); k < dim; k++){

					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

				}
			}
		}



}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	//#pragma omp parallel private(i,j,k) shared(A,B,C,dim) num_threads(omp_get_max_threads())
	//#pragma omp parallel for
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	//#pragma omp parallel for
	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)

				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void print_matrix (float *M, int dim){

	for(int i=0;i<dim;i++){

		for(int j=0;j<dim;j++){

			printf("%lf ",M[i*dim+j]);
		}

		printf("\n");
	}
	printf("\n");
	printf("\n");
}
