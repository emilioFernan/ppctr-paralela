# P4: Fractal de Mandelbrot

## Ejercicio 1

### Explica brevemente la funcion que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado (solo las que no hayas explicado en practicas anteriores).
     
     #pragma omp critical  -> Define una zona critica en el codigo, zona a la que solo puede acceder un hilo de manera simultanea
     omp_get_wtime() -> Retorna el valor en segundos de un lapso de tiempo 
     omp_set_lock(&lock) -> un lock puede estar en tres estados unintialized, unlocked, or locked, sirve para preservar la exclusion mutua de una region critica
     si se intenta hacer un lock cuando el lock esta en esta de locked, el proceso se bloquea (mutex).
     omp_unset_lock(&lock) -> sirve para desbloquear un lock y que de esta manera pueden hacer mas thread a la region critica 
     omp_init_lock(&lock) -> Inicializa un lock, es un error hacer un set o unset a un lock no inicializado
     
### Etiqueta todas las variables y explica por que le has asignado esa etiqueta.

    #pragma omp parallel private(i,j,x,y) shared(x_max,x_min,n,lock,c_max) 
    
    He puesto las variables i,j,x,y como privadas ya que estas se definen dentro de la region paralela y ademas puede tener distinto valor en distintos threads
    
    Las variables x_max,x_min,lock,c_max son compartidas ya que estan definidas previamente a la region paralela y ese mismo valor tiene que ser usado
    por varios threads.
    
### Explica brevemente como se realiza el paralelismo en este programa, indicando las partes que son secuenciales y las paralelas.
    
    En este programa lo que he hecho es paralelizar el bucle que determina count, de manera que se reparten las iteraciones de este y ademas comprobe que si en este bucle
    en vez de hacer llamar a la funcion explode se crean tareas de esta y despues se espera a que finalice tenemos una ganacia en el tiempo medio de respuesta ya que tarda menos de la siguiente manera:

```
  #pragma omp for
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

      y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);
      #pragma omp task
      count[i + j * n] = explode(x, y, count_max);
    }
  }

}
  #pragma omp taskwait
```
    
    De esta forma tenemos un tiempo medio de respuesta en torno a 8 con una n de 3200 mientras que de la otra forma, es decir, solo con el pragma for tenemos un tiempo de respuesta en torno a 12 para la misma n
    
    La region paralela en este caso abarca toda la main, el resto de funciones no las he paralelizado, intente paralelizar la funcion explode ya que es la que mas tiempo consume
    ,esto lo se gracia a gprof, pero al tener un break dentro del for es complicado hacerle una paralelizacion.
    
    El resto de bucles de la main tambien los he repartido mediante un #pragma for.
    Con los malloc no hay problema que esten dentro de la region paralela ya que los thread comparten esta memoria.
    El bucle  cuyo comentario  Determine the coloring of each pixel no lo he paralelizado ya que este comparte la variable como se dice en el ejercicio 2 
    por lo que en un principio usado un #pragma omp single de manera que este se hace de manera secuencial.
    
    
### Bonus optativo: utiliza alguna herramienta de pro ling (eg. gprof) para determinar que funciones deben ser optimizadas y/o paralelizadas. Explica como lo has usado y que decisiones has tomado. De cara a esta practica no pasa nada porque lo hagas una vez hayas paralelizado.

     He utilizado gprog para determinar que funciones optimizar los paso que segui fueron los siguientes:
     
     Lo primero que hice fue compilar mediante el flag "-pg" de la siguiente manera:
       
       gcc -pg -o pract4 mandelbrot.c -lm
       
     Una vez realizado esto, lo siguiente fue realizar el siguiente comando:
     
     gprof pract4 gmon.out > profile-data.txt
     
     Hecho esto si vamos al archivo profile-data.txt donde hemos volcado la información veremos lo siguiente:
![](images/gprof.png)

    Como se ve la funcion a la que más llamadas se realiza es a la de explode y tambien la que más tiempo consume por lo que habra que enfocar la paralelizacion/optimizacion 
    principalmente en esa funcion.

## Ejercicio 2

    a.  Implementacion mediante directivas OpenMP.  
    Hacemos uso de la directiva critical de la siguiente forma:
    
```
float inicio=omp_get_wtime();
  c_max = 0;
  #pragma omp for
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {

        {
        #pragma omp critical
        {
          c_max = count[i + j * n];
        }

      }

      }
    }
  }
  float segundos=omp_get_wtime();
```
    b.  Implementacion mediante funciones de runtime.

    Hacemos uso de las funciones omp_set_lock() y  omp_unset_lock(), a su vez seran necesarias
    una variable omp_lock_t e inicializarla mediante la funcion omp_init_lock
```
#pragma omp for
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {

        {
        omp_set_lock(&lock);
        c_max = count[i + j * n];
        omp_unset_lock(&lock);

      }

      }
    }
  }
```
    
    c.  Implementacion secuencial de esa parte del codigo.
    
    Para ello hacemos uso de la directiva single de OpenMP

```
float inicio=omp_get_wtime();
 
  #pragma omp single
  {
   c_max = 0;
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {

        {
          c_max = count[i + j * n];
        

      }

      }
    }
  }
}
  float segundos=omp_get_wtime();
```
    
    
    d.  Implementacion  con  variables privadas a cada  thread y seleccion del maximo de todas ellas.
    
    
    
Para hallar el tiempo de respuesta medio en cada caso, he usado el siguiente script para hallar los tiempos de respuesta de 10 ejecuciones en cada caso
```
file=pract4.log
if [ -f $file ];then
    rm pract4.log
fi
touch $file

gcc -std=c99 -fopenmp -o pract4 mandelbrot.c -lm

  ./pract4 1> /dev/null
  ./pract4 1> /dev/null
  ./pract4 1> /dev/null


for i in 1 2 3 4 5 6 7 8 9 10; do
      ./pract4 >> $file
done
```
    Una vez he obtenido los datos, los he tratado y he sacado las siguientes graficas:
    El eje vertical representa el tiempo en segundo mientras que el horizontal representa el numero de ejecucion, recuerdo que el script realiza 10 ejecuciones
![](images/chart.png)

   Vemos que para una n de 3200  los tiempos de los diferentes modos de asegurar la exclusion mutua son mi parejos y no podemos concretar si uno es mejor que otro.
   Al no sacar una conclusion clara probe para un tamaño de 6400 que es el tamaño que viene por defecto, a ver si esta manera encontraba alguna diferencia clara

   La grafica es la siguiente:
![](images/chart6400.png)

   Como vemos los resultados siguen siendo muy parecidos.
   A priori quizas el de single parece peor pero sin embargo los otros dos aunque haya varios hilos, estos se estan bloqueando por lo que los resultados obtenidos
   son coherentes.